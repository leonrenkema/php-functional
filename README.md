# ArrayList

A simple example of a wrapper around the basic PHP array's to provide some functional programming into PHP.

```php

$al = new ArrayList();

$al[] = 30;
$al[] = 50;

$newAl = $al->map(function($_) {
    return $_ % 2 = 0; 
});

```

## Build status

[![Build Status](https://travis-ci.org/leonrenkema/php-functional.png?branch=develop)](https://travis-ci.org/leonrenkema/php-functional)
