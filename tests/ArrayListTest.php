<?php

require_once 'PHPUnit/Framework/TestCase.php';

use ArrayList\ArrayList;

/**
 * ArrayList test case.
 */
class ArrayListTest extends PHPUnit_Framework_TestCase
{

    /**
     *
     * @var ArrayList
     */
    private $ArrayList;

    /**
     * Prepares the environment before running a test.
     */
    protected function setUp ()
    {
        parent::setUp();
        
        // TODO Auto-generated ArrayListTest::setUp()
        
        $this->ArrayList = new ArrayList(/* parameters */);
    }

    /**
     * Cleans up the environment after running a test.
     */
    protected function tearDown ()
    {
        // TODO Auto-generated ArrayListTest::tearDown()
        $this->ArrayList = null;
        
        parent::tearDown();
    }

    /**
     * Constructs the test case.
     */
    public function __construct ()
    {
        // TODO Auto-generated constructor
    }

    /**
     * Tests ArrayList->map()
     */
    public function testMap ()
    {
        $data = array(1,2,3,4,5,6,7,8);
        
        $isEven = function($_) {
            return $_ % 2 == 0;
        };
        $this->ArrayList->setData($data);
        
        $result = $this->ArrayList->map($isEven)->toArray();
        
        $reference = array(2,4,6,8);
        $this->assertEquals($result, $reference);
        
    }

    
    public function testArrayFeatures() {
        
        $this->ArrayList = new ArrayList();
        $this->ArrayList[8092] = "test";
        
        $this->assertArrayHasKey(8092, $this->ArrayList);
        $this->assertTrue($this->ArrayList[8092] == "test");
        unset($this->ArrayList[8092]);
        
        $this->assertFalse(array_key_exists(8092, $this->ArrayList));
    }
    
    
    /**
     * Tests ArrayList->count()
     */
    public function testCount ()
    {
        $this->ArrayList = new ArrayList();
        $this->assertTrue(count($this->ArrayList) == 0);
        
        $this->ArrayList[] = 10;
        $this->assertTrue(count($this->ArrayList) == 1);

        $this->ArrayList[] = null;
        $this->assertTrue(count($this->ArrayList) == 2);
        
        $this->ArrayList->count(/* parameters */);
    }

    /**
     * Tests ArrayList->sort()
     */
    public function testSort ()
    {
        $al2 = new ArrayList();
        $al2[] = 5;
        $al2[] = 2;
        $al2[] = 9;
        $al2[] = 10;
        $al2[] = 20;
        
        $al2->sort(function($a, $b) {
            if ($a == $b) return 0;
            if ($a < $b) return -1;
            return 1;
        });
        
        $reference = array(2,5,9,10,20);
        $this->assertEquals($reference, $al2->toArray());
    }

}

