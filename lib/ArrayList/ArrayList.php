<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ArrayList
 *
 * @author leonrenkema
 */

namespace ArrayList;

class ArrayList extends AbstractList {

    private $counter = 0;
    private $data = array();

    public function add($row) {
        $this->data[] = $row;
        $this->counter++;
    }

    public function get($index) {
        return $this->data[$index];
    }

    /**
     * Maps the ArrayList to a new ArrayList with the use of a function
     * @param function $func
     * @return ArrayList
     */
    public function map($func) {
        $resultArrayList = new ArrayList();

        // Create a new array to force the keys to begin with 0
        $resultArrayList->setData(array_values(array_filter($this->data, $func)));
        return $resultArrayList;
    }

    public function count() {
        return count($this->data);
    }

    public function sort($arg) {
        if (is_callable($arg)) {
            return $this->funcSort($arg);
        }
    }

    private function funcSort($func) {
        usort($this->data, $func);
        return $this->data;
    }

    /**
     * 
     * @param type $offset
     * @return type
     */
    public function offsetExists($offset) {
        return array_key_exists($offset, $this->data);
    }

    public function offsetGet($offset) {
        return $this->data[$offset];
    }

    public function offsetSet($offset, $value) {

        // $offset of NULL means that $object[] is used
        if ($offset === null) {
            $this->data[] = $value;
        } else {
            $this->data[$offset] = $value;
        }
    }

    public function offsetUnset($offset) {
        unset($this->data[$offset]);
    }

    public function setData($data) {
        $this->data = $data;
    }

    public function toArray() {
        return $this->data;
    }

    public function iterator() {
        return new ArrayListIterator($this);
    }

}
